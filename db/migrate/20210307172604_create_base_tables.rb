# frozen_string_literal: true

class CreateBaseTables < ActiveRecord::Migration[6.0]
  def change
    create_table :dealer_types do |t|
      t.string :name
      t.string :internal_identifier, null: false, index: true

      t.timestamps
    end

    create_table :dealers do |t|
      t.string :salesforce_id, index: { unique: true }
      t.string :name
      t.references :dealer_type, index: true

      t.timestamps
    end

    create_table :countries do |t|
      t.string :name
      t.string :code, null: false, index: { unique: true }

      t.timestamps
    end

    create_table :contacts do |t|
      t.string :street
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :phone_number
      t.decimal :lat, precision: 9, scale: 6
      t.decimal :lng, precision: 9, scale: 6
      t.references :country, index: true
      t.references :entity_record, polymorphic: true, index: true

      t.timestamps
    end
  end
end
