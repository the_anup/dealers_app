class AddMetaDataToDealers < ActiveRecord::Migration[6.0]
  def change
    add_column :dealers, :meta_data, :json, default: {}
  end
end
