# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_09_143310) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string "street"
    t.string "city"
    t.string "state"
    t.string "zipcode"
    t.string "phone_number"
    t.decimal "lat", precision: 9, scale: 6
    t.decimal "lng", precision: 9, scale: 6
    t.bigint "country_id"
    t.string "entity_record_type"
    t.bigint "entity_record_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["country_id"], name: "index_contacts_on_country_id"
    t.index ["entity_record_type", "entity_record_id"], name: "index_contacts_on_entity_record_type_and_entity_record_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "code", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_countries_on_code", unique: true
  end

  create_table "dealer_types", force: :cascade do |t|
    t.string "name"
    t.string "internal_identifier", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["internal_identifier"], name: "index_dealer_types_on_internal_identifier"
  end

  create_table "dealers", force: :cascade do |t|
    t.string "salesforce_id"
    t.string "name"
    t.bigint "dealer_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "meta_data", default: {}
    t.index ["dealer_type_id"], name: "index_dealers_on_dealer_type_id"
    t.index ["salesforce_id"], name: "index_dealers_on_salesforce_id", unique: true
  end

  create_table "seed_migration_data_migrations", id: :serial, force: :cascade do |t|
    t.string "version"
    t.integer "runtime"
    t.datetime "migrated_on"
  end

end
