class CreateBaseData < SeedMigration::Migration
  def up
    # create countries
    countries_file = File.read("#{Rails.root}/db/data/countries.json")
    countries = JSON.parse(countries_file)
    countries.each do |code, name|
      ::Country.create!(
        code: code,
        name: name
      )
    end

    # create dealer types
    ::DealerType.create!(
      name: 'Point Of Sale',
      internal_identifier: 'POS'
    )
  end

  def down
    ::Country.destroy_all
    ::DealerType.destroy_all
  end
end
