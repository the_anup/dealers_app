# README

Rails App to show Dealers which are synced to Salesforce


## Setup

Copy and update environment file
````
cp .env.development .env.development.local
````

````
bundle install
rake db:migrate
rake seed:migrate
yarn install
````

Load initial data from salesforce
````
rake salesforce:load_pos_dealers
````
Run server
````
rails s
````
Run salesforce sync subscriber
````
bin/salesforce_deamon run
````

## TODO:
- [x] Create Base tables
- [x] Add Rake Task for loading initial POS dealers
- [x] Create grape API for dealers index and show
- [x] Add the ability to sync dealers from Salesforce
- [x] Show dealers in google map
- [x] Show dealer details on hover of markers
- [x] Create list view for dealers
- [ ] Add tests
- [ ] Add the ability to broadcast changes dealer data changes to the Front End.




