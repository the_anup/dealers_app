# frozen_string_literal: true

namespace :salesforce do
  desc 'Load Dealers into the database'
  task :load_pos_dealers => :environment do |_t, args|
    batch_size = args[:batch_size].blank? ? 200 : args[:batch_size].to_i
    ::DataLoaderServices::Dealers::Load.run!(batch_size: batch_size)
  end
end
