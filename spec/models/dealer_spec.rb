# frozen_string_literal: true

require 'rails_helper'

describe Dealer, type: :model do
  describe 'associations' do
    it { should belong_to :dealer_type }
    it { should have_one :contact }
  end
end
