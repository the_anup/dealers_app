# frozen_string_literal: true

require 'rails_helper'

describe Contact, type: :model do
  describe 'associations' do
    it { should belong_to :country }
    it { should belong_to :entity_record }
  end
end
