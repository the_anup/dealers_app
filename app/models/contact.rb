# frozen_string_literal: true

class Contact < ApplicationRecord
  belongs_to :country
  belongs_to :entity_record, polymorphic: true
end
