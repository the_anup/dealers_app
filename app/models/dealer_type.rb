# frozen_string_literal: true

class DealerType < ApplicationRecord
  class << self
    def iid(internal_identifier)
      find_by(internal_identifier: internal_identifier)
    end
  end
end
