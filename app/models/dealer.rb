# frozen_string_literal: true

class Dealer < ApplicationRecord
  belongs_to :dealer_type
  has_one :contact, as: :entity_record, dependent: :destroy
end
