 # frozen_string_literal: true

module DataLoaderServices
  module Dealers
    class Sync < ::ActiveInteraction::Base
      integer :replay_id
      hash :payload do
        string :Id
        string :Name
        string :POS_Street__c
        string :POS_ZIP__c
        string :POS_Phone__c
        string :POS_Country__c
        string :POS_City__c
        string :POS_State__c
        float :Dealer_Latitude__c
        float :Dealer_Longitude__c
      end
      string :event_type
      boolean :create_if_not_found, default: true # option to re create a dealer if it gets deleted from the apps database

      def execute
        ActiveRecord::Base.transaction do
          send("on_#{event_type}_entity")
        end
      end

      private

      def on_created_entity
        dealer = ::Dealer.new(dealer_attrs)
        dealer.meta_data[:salesforce_replay_id] = replay_id
        dealer.build_contact(contact_attrs)
        dealer.save!
      end

      def on_updated_entity
        dealer = find_dealer
        return on_created_entity if dealer.nil? && create_if_not_found

        dealer.meta_data[:salesforce_replay_id] = replay_id
        dealer.update!(dealer_attrs)
        dealer.contact.update!(contact_attrs)
      end

      def on_deleted_entity
        dealer = find_dealer
        dealer.destroy! if dealer
      end

      def on_undeleted_entity
        on_created_entity
      end

      def find_dealer
        salesforce_id = payload[:Id]
        ::Dealer.find_by(salesforce_id: salesforce_id)
      end

      def dealer_attrs
        {
          salesforce_id: payload["Id"],
          name: payload["Name"],
          dealer_type: ::DealerType.iid('POS')
        }
      end

      def contact_attrs
        {
          street: payload["POS_Street__c"],
          city: payload["POS_City__c"],
          state: payload["POS_State__c"],
          zipcode: payload["POS_ZIP__c"],
          phone_number: payload["POS_Phone__c"],
          lat: payload["Dealer_Latitude__c"],
          lng: payload["Dealer_Longitude__c"],
          country: find_country
        }
      end

      def find_country
        code_or_name = payload["POS_Country__c"]
        ::Country.find_by("code = ? OR name = ?", code_or_name.upcase, code_or_name)
      end
    end
  end
end