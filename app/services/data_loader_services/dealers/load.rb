# frozen_string_literal: true

module DataLoaderServices
  module Dealers
    class Load < ::ActiveInteraction::Base
      integer :batch_size, default: 100

      def execute
        preload_countries
        preload_dealer_type
        fetch_dealers
        load_dealers
      end

      private

      def preload_countries
        @countries = ::Country.all.to_a
      end

      def preload_dealer_type
        @dealer_type = ::DealerType.iid('POS')
      end

      def fetch_dealers
        @dealers_collection ||= compose(
          ::AdapterServices::Salesforce::Account
        )
      end

      def load_dealers
        dealers = @dealers_collection.current_page
        create_dealers(dealers)

        np = @dealers_collection.next_page
        while np
          dealers = np.current_page
          create_dealers(dealers)
          np = np.next_page
        end
      end

      def create_dealers(dealers_data)
        dealers =
        ::Dealer.import(
          build_dealers(dealers_data),
          recursive: true,
          batch_size: batch_size
        )
      end

      def build_dealers(dealers_data)
        dealers_data.collect do |dealer_datum|
          build_dealer(dealer_datum)
        end
      end

      def build_dealer(dealer_datum)
        dealer = ::Dealer.new(
          dealer_attrs(dealer_datum),
        )
        build_dealer_contact(dealer, dealer_datum)
        dealer
      end

      def build_dealer_contact(dealer, dealer_datum)
        country = find_country(dealer_datum)
        dealer.build_contact(
          dealer_contact_attrs(dealer_datum, country)
        )
      end

      def find_country(dealer_datum)
        @countries.find do |c|
          # since I don't have access to on-running account in
          # test.salesforce.com I don't know how country is strored
          # hence accomodating for both name and code here.
          c.name == dealer_datum["POS_Country__c"] ||
          c.code == dealer_datum["POS_Country__c"].upcase
        end
      end

      def dealer_attrs(dealer_datum)
        {
          salesforce_id: dealer_datum["Id"],
          name: dealer_datum["Name"],
          dealer_type: @dealer_type
        }
      end

      def dealer_contact_attrs(dealer_datum, country)
        {
          street: dealer_datum["POS_Street__c"],
          city: dealer_datum["POS_City__c"],
          zipcode: dealer_datum["POS_ZIP__c"],
          country: country,
          state: dealer_datum["POS_State__c"],
          phone_number: dealer_datum["POS_Phone__c"],
          lat: dealer_datum["Dealer_Latitude__c"],
          lng: dealer_datum["Dealer_Longitude__c"]
        }
      end
    end
  end
end