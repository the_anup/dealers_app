# frozen_string_literal: true

module AdapterServices
  module Salesforce
    class CreatePushTopic < ActiveInteraction::Base
      string :name
      string :description
      boolean :notify_create, default: true
      boolean :notify_update, default: true
      boolean :notify_delete, default: true
      boolean :notify_undelete, default: true
      string :notify_fields, default: 'Referenced'
      string :query

      def execute
        create_push_topic
      end

      private

      def create_push_topic
        begin
        client.create!('PushTopic', push_topic_config)
        rescue Restforce::ErrorCode::DuplicateValue
          client
        end
        client
      end

      def push_topic_config
        {
          ApiVersion: '41.0',
          Name: name,
          Description: description,
          NotifyForOperationCreate: notify_create,
          NotifyForOperationUpdate: notify_update,
          NotifyForOperationDelete: notify_delete,
          NotifyForOperationUndelete: notify_undelete,
          NotifyForFields: notify_fields,
          query: query
        }
      end

      def client
        @client ||= compose ::AdapterServices::Salesforce::Client
      end

    end
  end
end