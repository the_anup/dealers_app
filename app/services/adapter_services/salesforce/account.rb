# frozen_string_literal: true

module AdapterServices
  module Salesforce
    class Account < ::ActiveInteraction::Base
      def execute
        fetch_pos_dealers
      end

      private

      def fetch_pos_dealers
        client.query(query_string)
      end

      def query_string
        <<-SQL.squish
            SELECT ID,
            Name,
            POS_Street__c,
            POS_City__c,
            POS_ZIP__c,
            POS_Country__c,
            POS_State__c,
            POS_Phone__c,
            Dealer_Latitude__c,
            Dealer_Longitude__c
            FROM Account
            WHERE E_Shop_Dealer__c = 'Dealer and Point of Sale'
          SQL
      end

      def client
        compose ::AdapterServices::Salesforce::Client
      end
    end
  end
end
