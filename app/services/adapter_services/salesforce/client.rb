# frozen_string_literal: true

module AdapterServices
  module Salesforce
    class Client < ::ActiveInteraction::Base
      def execute
        client
      end

      private

      def client
        client = ::Restforce.new
        client.authenticate!
        client
      end
    end
  end
end