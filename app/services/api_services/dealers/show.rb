# frozen_string_literal: true

module APIServices
  module Dealers
    class Show < ::ActiveInteraction::Base
      integer :id

      def execute
        relation.find_by(id: id).includes(:contact)
      end

      private

      def relation
        ::Dealer
      end
    end
  end
end
