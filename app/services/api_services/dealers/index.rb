# frozen_string_literal: true

module APIServices
  module Dealers
    class Index < ::ActiveInteraction::Base
      integer :page, default: 1
      integer :per_page, default: 10

      def execute
        dealers = relation.limit(page).offset(offset).includes(:contact)
        return_result(dealers)
      end

      private

      def return_result(dealers)
        {
          result: dealers,
          meta: meta
        }
      end

      def meta
        { total_count: relation.count }
      end

      def relation
        ::Dealer.where(dealer_type: ::DealerType.iid('POS'))
      end

      def offset
        (page - 1) * per_page
      end
    end
  end
end
