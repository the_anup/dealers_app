# frozen_string_literal: true

module API
  module V1
    module Dealers
      class Index < Base
        params do
          use :pagination
        end

        get '/' do
          index_params = declared(params)
          result = ::APIServices::Dealers::Index.run!(
            page: index_params[:page],
            per_page: index_params[:per_page]
          )
          present result[:result],
          with: ::DealerSerializer,
          include: [:contact, :'contact.country'],
          meta: result[:meta]
        end
      end
    end
  end
end
