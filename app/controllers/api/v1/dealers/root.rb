# frozen_string_literal: true

module API
  module V1
    module Dealers
      class Root < ::Grape::API
        mount Index
        mount Show
      end
    end
  end
end
