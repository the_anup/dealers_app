# frozen_string_literal: true

module API
  module V1
    module Dealers
      class Show < Base
        params do
          requires :id, type: Integer
        end

        get ':id' do
          show_params = declared(params)
          result = ::APIServices::Dealers::Show.run!(
            id: show_params[:id]
          )
          present result,
          with: ::DealerSerializer,
          include: [:contact, :'contact.country']
        end
      end
    end
  end
end
