# frozen_string_literal: true

module API
  module V1
    class Base < ::Grape::API
      helpers ::API::V1::Helpers::Params
    end
  end
end