# frozen_string_literal: true

module API
  module V1
    module Helpers
      module Params
        extend ::Grape::API::Helpers

        params :pagination do
          optional :page, type: Integer
          optional :per_page, type: Integer
        end

      end
    end
  end
end
