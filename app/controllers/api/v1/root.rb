# frozen_string_literal: true

module API
  module V1
    class Root < ::Grape::API
      format :json
      default_format :json
      content_type :json, 'application/json'
      formatter :json, ::Grape::Formatter::Json
      formatter :jsonapi, ::Grape::Formatter::Json
      # TODO: handle errors more granlarly
      rescue_from :all do |e|
        error!({ error: 'Server error.' }, 500, { 'Content-Type' => 'text/error' })
      end

      resource :dealers do
        mount ::API::V1::Dealers::Root
      end
    end
  end
end
