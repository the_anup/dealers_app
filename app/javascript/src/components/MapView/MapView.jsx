import React from 'react'
import PropTypes from 'prop-types'
import styles from './MapView.module.scss'
import useFetchFeaturesForMap from './useFetchFeaturesForMap'
import DealerIcon from './icons/dealer.svg'
import PlaceIcon from './icons/placeicon.svg'
import { Grid, Typography } from '@material-ui/core'

import GoogleMapReact from 'google-map-react'
const DEFAULT_ZOOM = 2
const DEFAULT_CENTER = { lat: 3, lng: 13.366667 } // fine-tuned to center in the "middle of earth"
const { GOOGLE_API_KEY } = process.env

const renderDealerMarkerInfo = ({
  name,
  street,
  city,
  state,
  zipcode,
  phoneNumber,
  country,
}) => {
  return (
    <div className={styles.info}>
      <Grid container justify="center">
        <Grid item xs={12} className={styles.item}>
          <Typography className={styles.name}>{name}</Typography>
        </Grid>
        <Grid container item xs={12}>
          <Grid item xs={1}>
            <PlaceIcon className={styles.placeIconStyles} />{' '}
          </Grid>
          <Grid item xs={10}>
            <Typography variant="caption">
              {street} {zipcode} {city} {state}
            </Typography>
            <Grid item xs={1}>
              <Typography variant="caption">{country}</Typography>
            </Grid>
            <Grid item xs={1}>
              <Typography variant="caption">{phoneNumber}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

const DealerMarker = ({
  $hover,
  id,
  name,
  street,
  city,
  state,
  zipcode,
  phoneNumber,
  country,
}) => {
  return (
    <div className={styles.marker}>
      <DealerIcon width={25} height={25} />
      {$hover &&
        renderDealerMarkerInfo({
          name,
          street,
          city,
          state,
          zipcode,
          phoneNumber,
          country,
        })}
    </div>
  )
}

DealerMarker.propTypes = {
  $hover: PropTypes.bool,
  id: PropTypes.number,
  name: PropTypes.string,
  street: PropTypes.string,
  city: PropTypes.string,
  state: PropTypes.string,
  zipcode: PropTypes.string,
  phoneNumber: PropTypes.string,
  country: PropTypes.string,
}

const MapView = () => {
  const [isDataFetched, data] = useFetchFeaturesForMap()
  return (
    <div className={styles.container}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_API_KEY }}
        defaultCenter={DEFAULT_CENTER}
        defaultZoom={DEFAULT_ZOOM}
      >
        {isDataFetched &&
          data.map((datum) => {
            return <DealerMarker key={datum.id} {...datum} />
          })}
      </GoogleMapReact>
    </div>
  )
}

export default MapView
