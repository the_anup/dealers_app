import { useEffect, useState } from 'react'

const useFetchFeaturesForMap = () => {
  const [isDataFetched, setIsDataFetched] = useState(false);
  const [data, setData] = useState([]);
  const googleMapsLoaded = window.google?.maps !== undefined

  useEffect(() => {
    if (isDataFetched) {
      return;
    }
    fetch('/v1/dealers/?page=1&per_page=100')
      .then((response) => response.json())
      .then((result) => {
        setIsDataFetched(true);
        setData(buildResult(result));
      }, [googleMapsLoaded, data, isDataFetched])
  });
  return [isDataFetched, data];
}

const buildResult = (result) => {
  const { data, included } = result
  return data.map((datum) => {
    const contactId = datum.relationships.contact.data.id
    const contactDatum = findContactDatum(contactId, included);
    const { city, state, lat, lng, phone_number, street, zipcode } = contactDatum.attributes;

    const countryId = contactDatum.relationships.country.data.id;
    const country = findCountryDatum(countryId, included).attributes.name;

    return {
      id: datum.attributes.id,
      name: datum.attributes.name,
      city: city,
      state: state,
      lat: lat,
      lng: lng,
      phoneNumber: phone_number,
      street: street,
      zipcode: zipcode,
      country: country
    }
  });
};

const findContactDatum = (contactId, includedDatum) => {
  return includedDatum.find(
    (attr) => attr.id === contactId && attr.type === 'contact',
  );
};

const findCountryDatum = (countryId, includedDatum) => {
  return includedDatum.find(
    (attr) => attr.id === countryId && attr.type === 'country',
  );
};

export default useFetchFeaturesForMap
