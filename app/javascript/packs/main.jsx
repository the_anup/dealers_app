import React from 'react'
import ReactDOM from 'react-dom'
import MapView from '../src/components/MapView'

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <MapView />,
    document.body.appendChild(document.createElement('div')),
  )
})
