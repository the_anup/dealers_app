# frozen_string_literal: true

class DealerSerializer < BaseSerializer
  attributes :id, :name
  has_one :contact
end
