# frozen_string_literal: true

class CountrySerializer < BaseSerializer
  attributes :code, :name
end