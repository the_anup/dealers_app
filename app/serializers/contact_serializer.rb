# frozen_string_literal: true

class ContactSerializer < BaseSerializer
  attributes :street, :city, :state, :zipcode, :phone_number
  attribute :lat do |object|
    object.lat.to_f
  end
  attribute :lng do |object|
    object.lng.to_f
  end

  belongs_to :country
end