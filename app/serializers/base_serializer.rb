# frozen_string_literal: true

class BaseSerializer
  include JSONAPI::Serializer
  class << self
    def represent(object, options = {})
      fields = (options[:fields] || {})
      new(object, options.merge(fields: fields)).serializable_hash
    end
  end
end