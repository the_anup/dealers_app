# load Rails
require_relative '../config/environment'


query_string = <<-SQL.squish
            SELECT ID,
            Name,
            POS_Street__c,
            POS_City__c,
            POS_ZIP__c,
            POS_Country__c,
            POS_State__c,
            POS_Phone__c,
            Dealer_Latitude__c,
            Dealer_Longitude__c
            FROM Account
            WHERE E_Shop_Dealer__c = 'Dealer and Point of Sale'
          SQL

client = ::AdapterServices::Salesforce::CreatePushTopic.run!(
  name: 'AllAccounts',
  description: 'All account records',
  query: query_string,
)
class ReplayHandler
  def initialize
    @replay_id = -1
  end

  def [](channel)
    # if we delete all dealers from the apps database then max_replay_id returns 0
    # fallback to recieving only new events then
    if @replay_id == 0
      @replay_id = -1
    else
      @replay_id
    end
  end

  def []=(channel, replay_id)
    replay_id = max_replay_id
  end

  private

  def max_replay_id
    Dealer.maximum("meta_data->>'salesforce_replay_id'").to_i
  end
end

EM.run do
  client.subscription '/topic/AllAccounts', replay: ReplayHandler.new do |message|
    begin
      puts message.inspect

      # wierd issue were latitude is coming as string but longitude is coming as float
      # so convert latitude from string to float and longitude too to be on a safer side
      message["sobject"]["Dealer_Latitude__c"] = message["sobject"]["Dealer_Latitude__c"].to_f
      message["sobject"]["Dealer_Longitude__c"] = message["sobject"]["Dealer_Longitude__c"].to_f

      ::DataLoaderServices::Dealers::Sync.run!(
        replay_id: message["event"]["replayId"],
        payload: message["sobject"].symbolize_keys,
        event_type: message["event"]["type"]
      )
    # we don't want the process to crash so just log exception
    rescue StandardError => err
      puts "***********Error****************"
      puts err.message
    end
  end
end
